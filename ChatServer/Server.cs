﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace ChatServer
{
    public class Server
    {
        public static void Main(string[] args)
        {
            // Set the TcpListener to port and local adress.
            Int32 port = 1200;
            System.Net.IPAddress localAddress = System.Net.IPAddress.Parse("127.0.0.1");

            TcpListener server = new TcpListener(localAddress, port);
            int requestCount = 0;
            TcpClient client = default;

            // start server
            server.Start();
            Console.WriteLine(" $ Server running...");
            client = server.AcceptTcpClient();
            Console.WriteLine(" $ Client connected...");
            requestCount = 0;
            
            // waiting on request
            while ((true))
            {
                try
                {
                    // counting requests in private member
                    requestCount += 1;

                    // get stream and read, retrieve message content without $
                    NetworkStream networkStream = client.GetStream();
                    byte[] bytesFrom = new byte[65536];
                    networkStream.Read(bytesFrom, 0, client.ReceiveBufferSize);
                    string dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));

                    // print to server console
                    Console.WriteLine(" $ Received a message from client in request " + requestCount.ToString() + " with content: " + dataFromClient);
                    
                    // send response
                    string response = "Message " + requestCount.ToString() + " from client with content: " + dataFromClient;
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(response);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();

                    // print to server console
                    Console.WriteLine(" $ " + response);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            // This never happens
            client.Close();
            server.Stop();
            Console.WriteLine(" $ exit");
            Console.ReadLine();
        }
        
    }
}